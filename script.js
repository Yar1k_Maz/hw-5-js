function createNewUser() {
   const firstName = prompt("Введіть ім'я:");
   const lastName = prompt("Введіть прізвище:");
 
   const newUser = {
     firstName: firstName,
     lastName: lastName,
     getLogin: function () {
       const firstLetter = this.firstName.charAt(0).toLowerCase();
       return firstLetter + this.lastName.toLowerCase();
     },
   };
 
   return newUser;
 }
 
 const user = createNewUser();
 const login = user.getLogin();
 console.log("Логін користувача:", login);
 